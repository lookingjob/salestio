# Test API

This is an API test

## Installing this software

* Clone project repository with:
```shell
$ git clone git@gitlab.com:lookingjob/salestio.git
$ cd salestio
```

* Install dependencies
```shell
$ composer install
```

* Prepare database
```shell
$ php bin/console doctrine:database:create
$ php bin/console doctrine:migrations:migrate
$ php bin/console rates:update
```

* Start server
```shell
$ symfony server:start
```
# API methods 

## Get cart total

### Request

`POST /cart/total`

    curl -i -H 'Accept: application/json' http://127.0.0.1:8000/cart/total

### Request body (RAW)
    {
        "items": {
            "42": {
                "currency": "EUR",
                "price": 49.99,
                "quantity": 1
            },
            "55": {
                "currency": "USD",
                "price": 12,
                "quantity": 3
            }
        },
        "checkoutCurrency": "EUR"
    }

### Response

    HTTP/1.1 200 OK
    Date: ...
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    Location: /cart/total
    Content-Length: ...

    {"checkoutPrice":83.04,"checkoutCurrency":"EUR"}

# Testing

* Prepare database:
```shell
$ php bin/console --env=test doctrine:database:create
$ php bin/console --env=test doctrine:schema:create
```

* Run tests:
```shell
$ php bin/phpunit
```

# License

The code is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
