<?php

namespace App\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use JsonException;

/**
 * CartController Test class.
 */
class CartControllerTest extends WebTestCase
{
    /**
     * @return void
     * @throws GuzzleException
     * @throws JsonException
     */
    public function testCartTotalPositive()
    {
        $requestData = [
            'items' => [
                '42' => [
                    'currency' => 'EUR',
                    'price' => 49.99,
                    'quantity' => 1
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR',
        ];

        $expectedResponseData = [
            'checkoutPrice' => 83.04,
            'checkoutCurrency' => 'EUR'
        ];

        $client = new Client([
            'base_uri' => 'http://localhost:8000',
            'timeout' => 0,
            'allow_redirects' => false,
        ]);

        $response = $client->post('/cart/total', [
            'body' => json_encode($requestData),
        ]);


        self::assertSame($response->getBody()->getContents(), json_encode($expectedResponseData, JSON_THROW_ON_ERROR));
        self::assertSame($response->getStatusCode(), 200);
    }

    /**
     * @return void
     * @throws GuzzleException
     */
    public function testCartTotalNegative()
    {
        $requestData = [
            '_items_' => [
                '42' => [
                    'currency' => 'EUR',
                    'price' => 49.99,
                    'quantity' => 1
                ],
                '55' => [
                    'currency' => 'USD',
                    'price' => 12,
                    'quantity' => 3
                ],
            ],
            'checkoutCurrency' => 'EUR',
        ];

        $client = new Client([
            'base_uri' => 'http://localhost:8000',
            'timeout' => 0,
            'allow_redirects' => false,
            'http_errors' => false
        ]);

        $response = $client->post('/cart/total', [
            'body' => json_encode($requestData),
        ]);

        self::assertSame($response->getStatusCode(), 400);
        self::assertArrayHasKey('message', $data = json_decode($response->getBody(), true));
    }
}