<?php

namespace App\Dto;

/**
 * Cart output DTO.
 */
class CartOutputDto
{
    /**
     * @var float
     */
    private float $checkoutPrice;
    /**
     * @var string
     */
    private string $checkoutCurrency;

    /**
     * @return float
     */
    public function getCheckoutPrice(): float
    {
        return $this->checkoutPrice;
    }

    /**
     * @param float $checkoutPrice
     */
    public function setCheckoutPrice(float $checkoutPrice): void
    {
        $this->checkoutPrice = $checkoutPrice;
    }

    /**
     * @return string
     */
    public function getCheckoutCurrency(): string
    {
        return $this->checkoutCurrency;
    }

    /**
     * @param string $checkoutCurrency
     */
    public function setCheckoutCurrency(string $checkoutCurrency): void
    {
        $this->checkoutCurrency = $checkoutCurrency;
    }
}