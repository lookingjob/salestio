<?php

namespace App\Dto;

use Exception;
use DateTimeImmutable;

/**
 * RateEntity DTO.
 */
class RateDto
{
    /**
     * @var string|float
     */
    public readonly string $rate;
    /**
     * @var DateTimeImmutable
     */
    public readonly DateTimeImmutable $updated_at;

    /**
     * @throws Exception
     */
    public function __construct(
        public readonly string $currency,
        string $rate,
        string $updated_at
    )
    {
        $this->rate = floatval($rate);
        $this->updated_at = new DateTimeImmutable($updated_at);
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }
}