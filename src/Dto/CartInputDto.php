<?php

namespace App\Dto;

/**
 * Cart input DTO.
 */
class CartInputDto implements InputInterface
{
    /**
     * @var CartItemDto[]
     */
    private array $items = [];

    /**
     * Class constructor.
     */
    public function __construct(
        array $items,
        private readonly string $checkoutCurrency
    ) {
        $this->items = $items;
    }

    /**
     * @return iterable
     */
    public function getItems(): iterable
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return $this
     */
    public function setItems(array $items): self
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @param CartItemDto $item
     * @return void
     */
    public function addItem(CartItemDto $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @param CartItemDto $item
     * @return void
     */
    public function removeItem(CartItemDto $item): void
    {
        //
    }

    /**
     * @return string
     */
    public function getCheckoutCurrency(): string
    {
        return $this->checkoutCurrency;
    }
}