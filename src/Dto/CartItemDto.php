<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints\Type;

/**
 * Cart item DTO.
 */
class CartItemDto
{
    /**
     * Class constructor.
     */
    public function __construct(
        private readonly string $currency,
        private readonly float $price,
        private readonly int $quantity
    ) {
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}