<?php

namespace App\Services;

use App\Dto\CartInputDto;
use App\Dto\CartOutputDto;
use App\Entity\RateEntity;
use UnexpectedValueException;

/**
 * Cart service class.
 */
class CartService
{
    /**
     * Class constructor.
     *
     * @param RateService $rateService
     */
    public function __construct(
        private readonly RateService $rateService
    ) {
    }

    /**
     * @param CartInputDto $cart
     * @return CartOutputDto
     */
    public function getTotal(CartInputDto $cart): CartOutputDto
    {
        if (empty($cart->getItems())) {
            throw new UnexpectedValueException('No cart items provided');
        }

        $total = 0;
        foreach($cart->getItems() as $cartItemDto) {
            $rate = $this->rateService->getByCurrency($cartItemDto->getCurrency());
            $total += $cartItemDto->getPrice() * (1 / $rate) * $cartItemDto->getQuantity();
        }

        $rate = $this->rateService->getByCurrency($cart->getCheckoutCurrency());

        $result = new CartOutputDto();
        $result->setCheckoutPrice(round($total * $rate, 2));
        $result->setCheckoutCurrency($cart->getCheckoutCurrency());

        return $result;
    }
}