<?php

namespace App\Services;

use App\Entity\RateEntity;
use App\Repository\RateRepository;
use UnexpectedValueException;

/**
 * Rate service class.
 */
class RateService
{
    /**
     * Class constructor.
     *
     * @param RateRepository $repository
     */
    public function __construct(
        private readonly RateRepository $repository
    ) {
    }

    /**
     * Getting rate for the currency name.
     *
     * @param string $currency
     * @return float
     */
    public function getByCurrency(string $currency): float
    {
        $rateEntity = $this->repository->getByCurrency($currency);

        if (!$rateEntity instanceof RateEntity) {
            throw new UnexpectedValueException(sprintf('Currency "%s" not found', $currency));
        }

        return $rateEntity->getRate();
    }
}