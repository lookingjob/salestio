<?php

namespace App\Services;

use App\Contracts\RatesProviderInterface;
use App\Dto\RateDto;
use App\Entity\RateEntity;
use OutOfBoundsException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Rates update service.
 */
class RatesUpdateService
{
    /**
     * Class constructor.
     *
     * @param RatesProviderInterface $ratesProvider
     * @param ValidatorInterface $validator
     * @param ManagerRegistry $doctrine
     */
    public function __construct(
        private readonly RatesProviderInterface $ratesProvider,
        private readonly ValidatorInterface $validator,
        private readonly ManagerRegistry $doctrine
    ) {
    }

    /**
     * @throws OutOfBoundsException
     */
    public function __invoke(): void
    {
        $rates[] = ($this->ratesProvider)();
        $rates = array_merge(...$rates);
        $this->store($rates);
    }

    /**
     * Store rates in database.
     *
     * @param RateDto[] $rates
     * @throws OutOfBoundsException
     */
    protected function store(array $rates): void
    {
        $entityManager = $this->doctrine->getManager();
        $validator = $this->validator;

        array_walk($rates, function (RateDto $rateDto) use ($entityManager, $validator): void {
            $rate = $entityManager->getRepository('App\Entity\RateEntity')->findOneBy([
                'currency' => $rateDto->currency,
            ]);
            if ($rate == null) {
                $rate = $this->createRateEntity($rateDto, $validator);
            } else {
                $rate->setRate($rateDto->rate);
            }

            $entityManager->persist($rate);
        });

        $entityManager->flush();
    }

    /**
     * Create new Rate entity from RateDto.
     *
     * @param RateDto $rateDto
     * @param ValidatorInterface $validator
     * @return RateEntity
     */
    private function createRateEntity(RateDto $rateDto, ValidatorInterface $validator): RateEntity
    {
        $rate = new RateEntity();
        $rate->setCurrency($rateDto->currency);
        $rate->setRate($rateDto->rate);
        $rate->setUpdatedAt($rateDto->updated_at);

        $errors = $validator->validate($rate);
        if (count($errors)) {
            throw new OutOfBoundsException((string)$errors);
        }

        return $rate;
    }
}