<?php

namespace App\Command;

use App\Services\RatesUpdateService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use RuntimeException;

/**
 * RatesUpdate Command.
 */
#[AsCommand(
    name: 'rates:update',
    description: 'Update exchange rates',
)]
class RatesUpdateCommand extends Command
{
    /**
     * Class constructor.
     *
     * @param RatesUpdateService $ratesUpdateService
     */
    public function __construct(private readonly RatesUpdateService $ratesUpdateService)
    {
        parent::__construct();
    }

    /**
     * @inheritDoc
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            ($this->ratesUpdateService)();
        } catch (RuntimeException $exception) {
            $io->error('An error occurred during rates updating: ');
            $io->text($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('Rates updated successfully.');

        return Command::SUCCESS;
    }
}