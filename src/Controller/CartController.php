<?php

namespace App\Controller;

use App\Dto\CartInputDto;
use App\Services\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\PartialDenormalizationException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use UnexpectedValueException;
use Symfony\Component\HttpClient\MockHttpClient;

/**
 * Cart controller class.
 */
class CartController extends AbstractController
{
    /**
     * Class constructor.
     *
     * @param SerializerInterface $serializer
     * @param CartService $cartService
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly CartService $cartService
    ) {
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    #[Route(path: '/cart/total', methods: ['post'])]
    public function __invoke(Request $request): JsonResponse
    {
        $content = $request->getContent();
        json_decode($content, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return $this->json(['message' => 'No valid JSON provided'], 400);
        }

        try {
            json_decode($content, true, 3, JSON_ERROR_SYNTAX);
        } catch (UnexpectedValueException $e) {
            return $this->json([
                'message' => $e->getMessage(),
            ], 400);
        }

        try {
            $cartDto = $this->serializer->deserialize($content, CartInputDto::class, 'json', [
                DenormalizerInterface::COLLECT_DENORMALIZATION_ERRORS => true
            ]);
        } catch (PartialDenormalizationException $e) {
            $violations = [];
            foreach ($e->getErrors() as $e) {
                if ($e->getExpectedTypes() && $e->getExpectedTypes()[0] !== 'unknown') {
                    $message = sprintf(
                        '%s: The type must be one of "%s" ("%s" given).',
                        $e->getPath(),
                        implode(', ', $e->getExpectedTypes()),
                        $e->getCurrentType()
                    );
                } else {
                    $message = sprintf('%s%s%s', $e->getPath(), $e->getPath() ? ': ' : '', $e->getMessage());
                }
                $violations[] = $message;

                break;
            }

            return $this->json(['message' => $violations], 400);
        }

        try {
            $cartTotal = $this->json($this->cartService->getTotal($cartDto));
        } catch (UnexpectedValueException $exception) {
            return $this->json([
                'message' => $exception->getMessage(),
            ], 400);
        }

        return $cartTotal;
    }
}
