<?php

namespace App\Repository;

use App\Entity\RateEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use UnexpectedValueException;

/**
 * Rate repository.
 *
 * @extends ServiceEntityRepository<RateEntity>
 *
 * @method RateEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method RateEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method RateEntity[]    findAll()
 * @method RateEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(private readonly ManagerRegistry $registry)
    {
        parent::__construct($registry, RateEntity::class);
    }

    /**
     * @param string $currency
     * @return ?RateEntity
     */
    public function getByCurrency(string $currency): ?RateEntity
    {
        $entityManager = $this->registry->getManager();

        return $entityManager->getRepository('App\Entity\RateEntity')->findOneBy([
            'currency' => $currency,
        ]);
    }
}
