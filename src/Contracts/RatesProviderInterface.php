<?php

namespace App\Contracts;

use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Rates provider interface.
 */
interface RatesProviderInterface
{
    /**
     * Class constructor.
     *
     * @param HttpClientInterface $client
     * @param string $url
     */
    public function __construct(HttpClientInterface $client, string $url);


    /**
     * Store rates to database.
     *
     * @return array
     */
    public function __invoke(): array;
}