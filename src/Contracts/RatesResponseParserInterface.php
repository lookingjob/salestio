<?php

namespace App\Contracts;

use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Response parser interface.
 */
interface RatesResponseParserInterface
{
    /**
     * Parse input data.
     *
     * @param ResponseInterface $response
     * @return array
     */
    public function parse(ResponseInterface $response): array;
}