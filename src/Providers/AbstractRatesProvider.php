<?php

namespace App\Providers;

use App\Dto\RateDto;
use App\Parsers\JsonRatesResponseParser;
use App\Contracts\RatesProviderInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Exception;
use UnexpectedValueException;

/**
 * Abstract rates providers class.
 */
abstract class AbstractRatesProvider implements RatesProviderInterface
{
    /**
     * Class constructor.
     *
     * @param HttpClientInterface $client
     * @param string $url
     */
    final public function __construct(
        private readonly HttpClientInterface $client,
        protected readonly string $url
    ) {
    }

    /**
     * @inheritDoc
     * @return array
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function __invoke(): array
    {
        $fetchResult = $this->fetch($this->url);
        $parsedData = $this->parse($fetchResult);

        return $this->transform($parsedData);
    }

    /**
     * Fetch all rates.
     *
     * @throws TransportExceptionInterface
     */
    protected function fetch(string $url): ResponseInterface
    {
        return $this->client->request(
            'GET',
            $url
        );
    }

    /**
     * Parse the response.
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    protected function parse(ResponseInterface $response): array
    {
        $contentType = $response->getHeaders()['content-type'][0];

        $parser = match ($contentType) {
            'application/json', 'application/javascript' => JsonRatesResponseParser::class,
            default => function () {
                throw new UnexpectedValueException('Unable to parse response');
            },
        };

        return (new $parser())->parse($response);
    }

    /**
     * Transform parsed result to array of exchange rates.
     *
     * @param array $data
     * @return array
     * @throws Exception
     */
    protected function transform(array $data): array
    {
        return array_map(
            static fn(array $rate): RateDto => new RateDto(
                $rate['currency'],
                $rate['rate'],
                $rate['updated_at']
            ),
            $data
        );
    }
}