<?php

namespace App\Providers;

/**
 * ExchangerateApi rates provider.
 */
class ExchangerateApiProvider extends AbstractRatesProvider
{
    /**
     * @inheritDoc
     * @throws \Exception
     */
    protected function transform(array $data): array
    {
        $rates = [];
        $updated_at = $data['time_last_update_utc'];

        foreach ($data['conversion_rates'] as $currency => $rate) {
            $rates[] = [
                'currency' => $currency,
                'rate' => $rate,
                'updated_at' => $updated_at,
            ];
        }

        return parent::transform($rates);
    }
}